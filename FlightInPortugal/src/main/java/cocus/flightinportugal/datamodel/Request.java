package cocus.flightinportugal.datamodel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;


public class Request
{
	@Id
	public long id;
	
	String dateFrom;
	String dateTo;
	List<String> destinations;
	String currency;
	
	public Request() {}
	
	public Request(long id, String dateFrom, String dateTo, String destA, String destB, String currency)
	{
		this.id = id;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		if(destA!=null || destB!=null)
		{
			destinations = new ArrayList<String>();
			if(destA!=null){destinations.add(destA);}
			if(destB!=null){destinations.add(destB);}
		}
		this.currency = currency;
	}

	public long getId(){return id;}
	
	public String getDateFrom() {return dateFrom;}
	
	public String getDateTo() {return dateTo;}
	
    public List<String> getDestinations() {return this.destinations;}

	public String getCurrency(){return currency;}
}
