
package cocus.flightinportugal.skypicker.msgmodel;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "flyFrom_type",
    "to_type",
    "seats"
})
public class SearchParams {

    @JsonProperty("flyFrom_type")
    private String flyFromType;
    @JsonProperty("to_type")
    private String toType;
    @JsonProperty("seats")
    private Seats seats;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public SearchParams() {
    }

    /**
     * 
     * @param toType
     * @param flyFromType
     * @param seats
     */
    public SearchParams(String flyFromType, String toType, Seats seats) {
        super();
        this.flyFromType = flyFromType;
        this.toType = toType;
        this.seats = seats;
    }

    @JsonProperty("flyFrom_type")
    public String getFlyFromType() {
        return flyFromType;
    }

    @JsonProperty("flyFrom_type")
    public void setFlyFromType(String flyFromType) {
        this.flyFromType = flyFromType;
    }

    @JsonProperty("to_type")
    public String getToType() {
        return toType;
    }

    @JsonProperty("to_type")
    public void setToType(String toType) {
        this.toType = toType;
    }

    @JsonProperty("seats")
    public Seats getSeats() {
        return seats;
    }

    @JsonProperty("seats")
    public void setSeats(Seats seats) {
        this.seats = seats;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("flyFromType", flyFromType).append("toType", toType).append("seats", seats).append("additionalProperties", additionalProperties).toString();
    }

}
