package cocus.flightinportugal.msgmodel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import cocus.flightinportugal.datamodel.Request;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListReply
{	
	@JsonProperty("msg")
	private String msg;
	
	@JsonProperty("requests")
	private List<Request> requests;

	public ListReply(String msg){this.msg=msg;}

	@JsonProperty("msg")
	public String getMsg(){return msg;}
	
	@JsonProperty("requests")
	public List<Request> getRequests(){return requests;}
	
	public void setRequests(List<Request> requests)
	{
		if(requests!=null && requests.size()>0){this.requests = requests;}
	}
}
