package cocus.flightinportugal.datamodel;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends MongoRepository<Request, String>
{
	public Optional<Request> findById(String id);
}