package cocus.flightinportugal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import cocus.flightinportugal.datamodel.Request;
import cocus.flightinportugal.datamodel.RequestRepository;
import cocus.flightinportugal.msgmodel.ListReply;
import cocus.flightinportugal.msgmodel.Reply;
import cocus.flightinportugal.skypicker.msgmodel.Datum;
import cocus.flightinportugal.skypicker.msgmodel.Search;

@RestController
@EnableMongoRepositories
public class FlightInPortugalController
{
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	RequestRepository repository;
	  
	private static final Logger log = LoggerFactory.getLogger(FlightInPortugalController.class);
	
	private final String baseURL = "https://api.skypicker.com/flights?";

	@GetMapping("/flght/list")
	public ListReply listRecords()
	{
		List<Request> reqs = repository.findAll();
		ListReply listReply = new ListReply("A toltal of " + reqs.size() + " records in store.");
		listReply.setRequests(reqs);
		
		return listReply;
	}
	
	@GetMapping("/flght/del")
	public ListReply deleteRecords()
	{
		repository.deleteAll();
		List<Request> reqs = repository.findAll();
		ListReply listReply = new ListReply("No records in store.");
		listReply.setRequests(reqs);
		
		return listReply;
	}
	
	@GetMapping("/flght/avg")
	public Reply avg(
			@RequestParam(value = "dateFrom", required = false) String dateFrom,
			@RequestParam(value = "dateTo", required = false) String dateTo,
			@RequestParam(value = "dest", required = false) List<String> destinations,
			@RequestParam(value = "curr", defaultValue = "EUR") String currency
			)
	{
		Reply reply = null;
		long reqID = getID();
		
		//if no destination is provided the information is retrieved for both Oporto->Lisbon and Lisbon->Oporto flights
		if(destinations == null || destinations.size()<1)
		{
			destinations = new ArrayList<String>();
			destinations.add("OPO");
			destinations.add("LIS");
		}
		
		StringBuilder resultMsg = new StringBuilder();
		if(!validateParameters(dateFrom,dateTo,destinations,currency,resultMsg))
		{
			log.info("One or more of the provided arguments is inadequately expressed.");
			reply = new Reply(reqID, resultMsg.toString()); 
		}
		else
		{
			log.info("Receiving request for data on flights from date " + dateFrom + " to date= " + dateTo + " with prices in " + currency);
			
			//persist request information to MongoDB
			persistRequestInfo(reqID, dateFrom,dateTo,destinations,currency);
			
			//normalise arguments
			destinations = normalizeCityNames(destinations);
			
			//build search query(ies)
			HashMap<String,String> searchQueries = buildSearchQueries(baseURL,destinations,dateFrom,dateTo,currency);
			printIterableList(searchQueries.entrySet(),"SearchQueries");
			
			//retrieve data from skypicker service 
			log.info("Retrieving information form https://api.skypicker.com");
			HashMap<String,Object> searchResults = querySkypicker(searchQueries);
			
			//save the search results to file(s)
			saveSearchResults(searchResults,"C:/Users/Helder/Desktop/");
			
			log.info("Building reply msg");
			reply = buildReply(searchResults,reqID,dateFrom, dateTo,currency);

		}
		
		return reply;
	}

    private boolean validateParameters(String dateFrom, String dateTo, List<String> destinations, String currency, StringBuilder results)
	{
		boolean valid = true;
		
		long fromDateLong = 0;
		long toDateLong = 0;
		
		if(dateFrom!=null)
		{
			try {fromDateLong = dateStringToTimeInMillis(dateFrom);}
			catch (ParseException e)
			{
				results.append("The fromDate parameter contains an inadequate value " + dateFrom + ".");
				valid = false;
			}
		}
		
		if(dateTo!=null)
		{
			try
			{
				toDateLong = dateStringToTimeInMillis(dateTo);
			}
			catch (ParseException e)
			{
				results.append("The toDate parameter contains an inadequate value " + dateTo + ".");
				valid = false;
			}
		}
	
		if(valid && (toDateLong!=0 && fromDateLong!=0) && (toDateLong<=fromDateLong))
		{
			results.append("The specified toDate (" + dateTo + ") is earlier or the same as the specified fromDate (" + dateFrom + ").");
			valid = false;
		}
		
		if(destinations.size()<1 || destinations.size()>2)
		{
			results.append("Inadequate number of destinations specified (" + destinations.size() + ").");
			valid = false;
		}
		else
		{
			Iterator<String> destinationIterator = destinations.iterator();
			while(destinationIterator.hasNext())
			{
				String destination = destinationIterator.next();
				if(!isEitherPortoOrLisbon(destination))
				{
					results.append("One of the specified destination city(ies) (" + destination + ") is neither Porto nor Lisbon.");
					valid = false;
					break;
				}
			}
		}
		
		return valid;
	}

	private void persistRequestInfo(long reqID, String dateFrom, String dateTo, List<String> destinations, String currency)
	{
		String destA = null;
		String destB = null;
		Iterator<String> destinationIterator = destinations.iterator();
		while(destinationIterator.hasNext())
		{
			String dest = destinationIterator.next();
			if(destA==null) {destA=dest;continue;}
			if(destB==null)
			{
				destB=dest;
				break;
			}
			else {break;}
		}
		Request request = new Request(reqID, dateFrom,dateTo,destA,destB,currency);
		repository.save(request);
	}

	private List<String> normalizeCityNames(List<String> destinations)
    {
    	List<String> normalizedDests = new ArrayList<String>(); 
		Iterator<String> destinationIterator = destinations.iterator();
		while(destinationIterator.hasNext())
		{
			String destCity = destinationIterator.next();
			if(isLisbon(destCity)) {destCity="LIS";}
			else if(isPorto(destCity)) {destCity="OPO";}
			normalizedDests.add(destCity);
		}
		return normalizedDests;
	}

	private HashMap<String,String> buildSearchQueries(String baseURL, List<String> destinations, String fromDate, String toDate, String currency)
	{
		HashMap<String,String> searchQueries = new HashMap<String,String>();
		
		Iterator<String> destinationIterator = destinations.iterator();
		while(destinationIterator.hasNext())
		{
			String destinationCity = destinationIterator.next();
	
			searchQueries.put(destinationCity,buildSearchQuery(baseURL,destinationCity, fromDate, toDate, currency));
		}
		
		return searchQueries;
	}

	private String buildSearchQuery(String baseURL, String destinationCity, String fromDate, String toDate, String currency)
	{		
		StringBuilder searchQueryBuilder = new StringBuilder(baseURL);
		
		String departureCity = "LIS";
		if(isLisbon(destinationCity)) {destinationCity="LIS";departureCity="OPO";}
		else if(isPorto(destinationCity)) {destinationCity="OPO";}
	
		searchQueryBuilder.append("flyFrom=" + departureCity + "&to=" + destinationCity);
		if(fromDate!=null) {searchQueryBuilder.append("&dateFrom=" + fromDate);}
		if(toDate!=null) {searchQueryBuilder.append("&dateTo=" + toDate);}
		searchQueryBuilder.append("&partner=picky");
		if(currency!=null) {searchQueryBuilder.append("&curr=" + currency);}
		
		return searchQueryBuilder.toString();
	}

	private HashMap<String,Object> querySkypicker(HashMap<String,String> searchQueries)
    {
		HashMap<String,Object> searchResults = new HashMap<String,Object>();

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Accept", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);

	    Iterator<Entry<String,String>> searchQueryIterator = searchQueries.entrySet().iterator();
	    while (searchQueryIterator.hasNext())
	    {
	        Map.Entry<String,String> pair = (Map.Entry<String,String>) searchQueryIterator.next();
	        String destName = pair.getKey();
	        String searchQuery = pair.getValue();
	        
			//Search searchResult = restTemplate.getForObject(searchQuery, Search.class);
	        //searchResults.put(destName,searchResult);
	        
			//ResponseEntity<Search> searchResult = restTemplate.exchange(searchQuery, HttpMethod.GET, httpEntity, Search.class);
	        //searchResults.put(destName,searchResult.getBody());

	    	try
	    	{
	    		ResponseEntity<String> response = restTemplate.exchange(searchQuery,HttpMethod.GET, httpEntity, String.class);
			    if (response.getStatusCode().equals(HttpStatus.OK))
			    {
					Search searchResult = new ObjectMapper().readValue(response.getBody(), Search.class);
					searchResults.put(destName,searchResult);
			    }
			    else
			    {
			    	searchResults.put(destName,response.getBody());
			    }
			}
	    	catch (HttpClientErrorException e)
	    	{
	    		searchResults.put(destName,e.getResponseBodyAsString());
			}
	    	catch (JsonMappingException e)
	    	{
				e.printStackTrace();
				searchResults.put(destName,null);
			}
	    	catch (JsonProcessingException e)
	    	{
				e.printStackTrace();
				searchResults.put(destName,null);
			}
	    }
		
    	return searchResults;
	}

	private void saveSearchResults(HashMap<String,Object> searchResults, String outputDirPath)
	{
	    Iterator<Entry<String,Object>> searchResultIterator = searchResults.entrySet().iterator();
	    while (searchResultIterator.hasNext())
	    {
	        Map.Entry<String,Object> pair = (Map.Entry<String,Object>) searchResultIterator.next();
	        String destName = pair.getKey();
	        Object searchResult = pair.getValue();
	       
		    ObjectMapper mapper = new ObjectMapper();
		    mapper.enable(SerializationFeature.INDENT_OUTPUT);
		    File outputJsonFile = new File(outputDirPath + destName + "_out.json");
	
			try
			{
			    // Convert object to JSON string
			    //String jsonStr = mapper.writeValueAsString(search);
			    //System.out.println(jsonStr);
	
			    // Save JSON string to file
			    FileOutputStream fileOutputStream = new FileOutputStream(outputJsonFile);
			    mapper.writeValue(fileOutputStream, searchResult);
			    fileOutputStream.close();
			}
			catch (JsonProcessingException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
	    }
	}

	private Reply buildReply(HashMap<String,Object> searchResults, long id, String fromDate, String toDate, String currency)
	{
		Reply reply = new Reply(id);
		
	    Iterator<Entry<String,Object>> searchResultIterator = searchResults.entrySet().iterator();
	    while (searchResultIterator.hasNext())
	    {
	        Map.Entry<String,Object> pair = (Map.Entry<String,Object>) searchResultIterator.next();
	        String destName = pair.getKey();
	        Object result = pair.getValue();
	        
	        //a reply was successfully received from Skypicker
	        if(result!=null && result instanceof Search)
	        {
		        Search searchResult = (Search) result;
				List<Datum> dataList = searchResult.getData();
				log.info("A total of " + searchResult.getData().size() + " flights were found");
				
			    List<Integer> flightPrices = new ArrayList<Integer>();
			    List<Double> frstBagPrices = new ArrayList<Double>();
			    List<Double> scndBagPrices = new ArrayList<Double>();
				
			    long fromDateLong = 0;
			    long toDateLong = 0;
			    
			    int targetFlightCounter = 0;
				Iterator<Datum> dataListIterator = dataList.iterator();
				while(dataListIterator.hasNext())
				{
					Datum datum = dataListIterator.next();
					List<String> airlines = datum.getAirlines();
					if(isOneOfTheAirlinesEitherTAPorRyanair(airlines))
					{
						targetFlightCounter = targetFlightCounter + 1;
						
						Integer flightPrice = datum.getPrice();
						if(flightPrice!=null) {flightPrices.add(flightPrice);}
						//else {log.info("Null value found for flightPrice at " + datum.getId());}
						
						Double frstBagPrice = datum.getBagsPrice().get1();
						if(frstBagPrice!=null) {frstBagPrices.add(frstBagPrice);}
						//else {log.info("Null value found for frstBagPrice at " + datum.getId());}
						
						Double sndBagPrice = datum.getBagsPrice().get2();
						if(sndBagPrice!=null) {scndBagPrices.add(sndBagPrice-frstBagPrice);}
						//else {log.info("Null value found for sndBagPrice at " + datum.getId());}
						
						if(fromDate==null)
						{
							long time = datum.getDTime()*1000L;
							log.info("dTimeA = " + datum.getDTime() + " *1000 = " + time);
							if(fromDateLong==0 || time<fromDateLong){fromDateLong=time;}
						}
						if(toDate==null)
						{
							long time = datum.getDTime()*1000L;
							log.info("dTimeB = " + datum.getDTime() + " *1000 = " + time);
							if(toDateLong==0 || time>toDateLong){toDateLong=time;}
						}
					}
				}
				log.info("Information was retrieved on " + dataList.size() + " flights. " + targetFlightCounter + " of these where from TAP or Ryanair");
				log.info("A total of " + flightPrices.size() + " flight prices, " + frstBagPrices.size() + " first bag prices, and " + scndBagPrices.size() + "second bag prices were retrieved.");
				
				//log.info("flightPrices has " + flightPrices.size() + " elements.");
				//log.info("frstBagPrices has " + frstBagPrices.size() + " elements.");
				//log.info("scndBagPrices has " + scndBagPrices.size() + " elements.");
				
				Integer avgFlightPrice = (Integer) (new Averager<Integer>(flightPrices).getAverage());
				Integer avgFrstBagPrice = round((Double) new Averager<Double>(frstBagPrices).getAverage());
				Integer avgScndBagPrice = round((Double) new Averager<Double>(scndBagPrices).getAverage());
				
				log.info("fromDateLong " + fromDateLong);
				log.info("toDateLong " + toDateLong);
				
				if(fromDate==null){fromDate=timeInMillis2DateString(fromDateLong);}
				if(toDate==null){toDate=timeInMillis2DateString(toDateLong);}
				
				if(destName.equals("OPO")) {reply.setOPO(avgFlightPrice, avgFrstBagPrice, avgScndBagPrice, fromDate, toDate, "Francisco Sa Carneiro", currency);}
				else {reply.setLIS(avgFlightPrice, avgFrstBagPrice, avgScndBagPrice, fromDate, toDate, "Humberto Delgado", currency);}
	        }
	        else //an error msg was received from Skypicker
	        {
	        	String msg = null;
	        	if(result!=null) {msg=(String) result;}
	        	else {msg="An error occurred in the Skypicker service or in the interaction with it.";}
	        	
				if(destName.equals("OPO")) {reply.setOPO(msg);}
				else {reply.setLIS(msg);}
	        }
	    }
	
		return reply;
	}

	private boolean isEitherPortoOrLisbon(String city)
	{
		boolean result = false;
		
		if(isPorto(city) || isLisbon(city)) {result = true;}
		
		return result;
	}

	private boolean isLisbon(String city)
	{
		boolean result = false;
		if(city.equalsIgnoreCase("Lisbon") || city.equalsIgnoreCase("LIS") || city.equalsIgnoreCase("Lisboa") || city.equalsIgnoreCase("LPPT")){result=true;}
		return result;
	}

	private boolean isPorto(String city)
	{
		boolean result = false;
		if(city.equalsIgnoreCase("Oporto") || city.equalsIgnoreCase("OPO") || city.equalsIgnoreCase("Porto") || city.equalsIgnoreCase("LPPR")){result=true;}
		return result;
	}

	private boolean isTAPCode(String code) 
	{
		boolean result = false;
		
		String IATA_Code = "TP";
		String ICAO_Code = "TAP";
		String Prefix_Code = "047";
		
		if(IATA_Code.equals(code) || ICAO_Code.equals(code) || Prefix_Code.equals(code)) {result=true;}
		
		return result;
	}
	
	private boolean isOneOfTheAirlinesEitherTAPorRyanair(List<String> airlines)
	{
		boolean result = false;
		Iterator<String> airlinesIterator = airlines.iterator();
		while(airlinesIterator.hasNext())
		{
			String airlineCode = airlinesIterator.next();
			if(isTAPCode(airlineCode.trim()) || isRyanairCode(airlineCode.trim())) 
			{
				result = true;
				break;
			}
		}
		return result;
	}

	private boolean isRyanairCode(String code) 
	{
		boolean result = false;
		
		String IATA_Code = "FR";
		String ICAO_Code = "RYR";
		
		if(IATA_Code.equals(code) || ICAO_Code.equals(code)) {result=true;}
		
		return result;
	}

	private String timeInMillis2DateString(long timeInMillis)
	{
		String dateStr = "";
		Calendar calendar = timeInMillis2Calendar(timeInMillis);
		
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);
		
		dateStr = String.valueOf(mDay) + "/" + String.valueOf(mMonth) + "/" + String.valueOf(mYear);
		
		return dateStr;
	}
	
	private Calendar timeInMillis2Calendar(long timeInMillis)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timeInMillis);
		return calendar;
	}

	private long dateStringToTimeInMillis(String dateStr) throws ParseException
	{
		//Date string format loike 2014/10/29
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(dateStr);
		return dateToTimeInMillis(date);
	}
	
	private long dateToTimeInMillis(Date date)
	{
		long millis = date.getTime();
		return millis;
	}
	
	private <S, T extends Iterable<S>> void printIterableList(T list, String msg)
	{
		try
		{
	    	System.out.println(msg);
	        for (Object element : list){log.info(element.toString());}
		}
		catch (Exception e)
		{
			log.error("Error while attempting to print an Iterable list");
		}
	
	}

	private Integer round(double value)
	{
	    BigDecimal number = BigDecimal.valueOf(value);
	    number = number.setScale(0, RoundingMode.HALF_UP);
	    return number.intValue();
	}
	
	private static synchronized long getID(){return System.currentTimeMillis();}
	
	class Averager<T extends Number>
    {
        private List<T> listOfQuantities;
        private Object  average;

        @SuppressWarnings("unchecked")
        public Averager(List<T> list)
        {
            this.listOfQuantities = list;

            T sampleValue = listOfQuantities.get(0);
            if (sampleValue instanceof Integer)
            {
                calculateIntegerAverage((List<Integer>) listOfQuantities);
            }
            else if(sampleValue instanceof Double)
            {
                calculateDoubleAverage((List<Double>) listOfQuantities);
            } 
            else if (sampleValue instanceof Float)
            {
                calculateFloatAverage((List<Float>) listOfQuantities);
            }
            else
            {
                throw new IllegalStateException("The Averager's constructor must be initialized with a List<T> of Integer, Double or Float");
            }
        }

        private void calculateDoubleAverage(List<Double> listOfQuantities)
        {
            Double total = Double.valueOf(0);
            for(Double amount : listOfQuantities){total += amount;}
            average = total/Double.valueOf(listOfQuantities.size());
        }

        private void calculateIntegerAverage(List<Integer> listOfQuantities)
        {
            Integer total = Integer.valueOf(0);
            for(Integer amount : listOfQuantities){total += amount;}
            average = total/Integer.valueOf(listOfQuantities.size());
        }

        private void calculateFloatAverage(List<Float> listOfQuantities)
        {
            Float total = Float.valueOf(0);
            for(Float amount : listOfQuantities){total += amount;}
            average = total/Float.valueOf(listOfQuantities.size());
        }

        Object getAverage(){return average;}
    }
	
}